const express = require('express');
const fs = require (`fs`);
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true}));
const port = 3000;
const manejaGet = (req, res) => {
    console.log("Estoy en la pagina home");
    res.json({Saludo: `Hola mundo desde Node!`});
}
app.get('/home', manejaGet);

app.get(`/ElisaCV`, (req, res) => {
    console.log(`Get /ElisaCV`);
    res.json({Name: `Elisa Coloriano Victorio`});
});

const readFile = (err, data) => {
    if (err) console.log(`Hubo un error`);
    let info = JSON.parse(data);
    console.log(info);
    return info;
}

app.get(`/People`, (req, res) => {
    let data = fs.readFileSync(`database/table.json`);
    let info = JSON.parse(data);
    console.log(`Leimos el archivo`);
    console.log(info);
    res.json({Response: info})
})

app.get(`/People/:Id`, (req, res) => {
    let identifier = req.params.Id;
    let data = fs.readFileSync(`database/table.json`);
    let obJSON = JSON.parse(data);
    let response = null;
    for (let i = 0; i < obJSON.length; i++){
        if (obJSON[i].Id == identifier){
            response = obJSON[i];
            break;
        }
    }
    if(response == null){
        res.status(404).send();
        return;
    }
    res.json(response);
})

app.post(`/People`, (req, res) => {
    console.log(req.body);
    let person = req.body;
    let data = fs.readFileSync(`database/table.json`);
    let info = JSON.parse(data);

    for(let i=0; i>info.length; i++){
        if(info[i].Id==person.Id){
            res.status(400).json({Mensaje: `El usuario ya existe`});
            return;
        }
    }
    info.push(person);
    fs.writeFileSync(`database/table.json`, JSON.stringify(info));
    res.status(201).json(person);
})

app.listen(port, () => {
    console.log("El servidor esta escuchando en la url http://localhost:", port);
})